package example;

public class Application {
	private static final String HELLO_WORLD_OUTPUT = "Hello World!";

	public static void main(String[] args) {
        System.out.println(HELLO_WORLD_OUTPUT);
    }
}
